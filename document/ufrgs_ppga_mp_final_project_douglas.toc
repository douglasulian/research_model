\changetocdepth {4}
\select@language {brazil}
\select@language {english}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introduction}}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{12}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}General Objectives}{12}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Specific Objectives}{12}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Study relevance}{13}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Grupo RBS, Zero Hora and the Media Business in Brazil}}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Media Business in Brazil}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}Grupo RBS}{15}{section.2.2}
\contentsline {section}{\numberline {2.3}Zero Hora}{16}{section.2.3}
\contentsline {section}{\numberline {2.4}Customer Experience at Zero Hora}{16}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Related Work}}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}News Data Mining}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}News Recommendation Systems}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Collaborative Filtering}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Content-based Filtering}{21}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Hybrid approaches}{21}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Main Challenges}{21}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Cluster Analysis}{22}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Measure of Similarity}{24}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Number of Clusters}{26}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Clusters Validation}{27}{subsection.3.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Method}}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Objects Selection}{30}{section.4.1}
\contentsline {section}{\numberline {4.2}Variable Selection}{30}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}User Data Variables}{30}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Behaviour Data Variables}{31}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Content Data Variables}{31}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Proximity Measure}{33}{section.4.3}
\contentsline {section}{\numberline {4.4}Cluster Method and Evaluation}{34}{section.4.4}
\contentsline {section}{\numberline {4.5}Replication and Testing}{34}{section.4.5}
\contentsline {section}{\numberline {4.6}System Overview}{35}{section.4.6}
\contentsline {section}{\numberline {4.7}Timeline}{38}{section.4.7}
\vspace {\cftbeforechapterskip }
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Bibliography}}{39}{section*.6}
\vspace {\cftbeforechapterskip }
