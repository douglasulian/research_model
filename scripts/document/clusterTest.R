list.of.packages = c('dplyr','ClusterR','cluster','factoextra','NbClust','foreach','doParallel','parallel','xtable')
new.packages = list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if (length(new.packages)) install.packages(new.packages)

library(cluster)
library(ClusterR)
library(dplyr)
library(ggplot2)
library(factoextra)
library(NbClust)
library(foreach)
library(doParallel)
library(parallel)
library(tibble)
library(xtable)

debugSource(fileName = 'scripts//cache.R')
debugSource(fileName = 'scripts//dataBase.R')
debugSource(fileName = 'scripts//distances.R')
debugSource(fileName = 'scripts//cluster.R')

clearConnections()

trainningConnection = getDataBaseConnection(schema = 'trainning2000', dbUser = "cl-us-gzh", dbHost = "10.238.4.109", dbName = "cl-us-gzh", dbPass = "cl-us-gzh")

trainningData       = getTrainningData(trainningConnection)

lambdaIndex = 1
alphaIndex = 1
betaIndex = 0.5
times = 100

plotTitleSize = 21+10
axisTextSize  = 18+10
axisTitleSize = 21+10

distances = getDistances(trainningData, lambdaIndex, alphaIndex, betaIndex)

matrixDistance = distances$mixedDistance
usersTags      = distances$usersTagsMatrix

rm('distances')

gc()

test = list()
plotsDir = 'document_eswa//plots//'
tablesDir = 'document_eswa//tables//'
relevantLimit = 20
methods = c('ward.D', 'ward.D2', 'average', 'single', 'complete', 'mcquitty', 'pam', 'kmedoid')
for (i in 2:times) {
  print(i)
  for (name in methods){
    time = system.time({
      clusterResult = getCluster(distance = matrixDistance, k = i, method = name) 
    })
    clusterResult = as.tibble(clusterResult$cluster$cluster) %>% mutate(cluster = as.integer(cluster)) %>% select(cluster)
    test$cluster = c(test$cluster,clusterResult)
    test$k = c(test$k,i)
    test$name = c(test$name,name)
    test$userTime = c(test$userTime,time[1])
    test$systemTime = c(test$systemTime,time[2])
    test$elapsedTime = c(test$elapsedTime,time[3])
    test$silhouette = c(test$silhouette,(summary(silhouette(x = pull(clusterResult),dmatrix = matrixDistance)))$si.summary[4])
    if (name == 'pam' || name == 'kmedoid') type = 'optimization' else type = 'hierarchical'
    test$type = c(test$type, type)
  }
}
test$k
test$relevants = c()
for (i in 1:length(test$cluster)){
  test$relevants = c(test$relevants,sum((as.tibble(test$cluster[i]) %>% group_by(cluster) %>% count(cluster))[,2] > 20))  
}
test$name[test$name == 'kmedoid'] = "K-Medoids"
test$name[test$name == 'pam'] = "PAM"
test$name[test$name == 'ward.D'] = "Ward.D"
test$name[test$name == 'ward.D2'] = "Ward.D2"
test$name[test$name == 'average'] = "Average"
test$name[test$name == 'single'] = "Single"
test$name[test$name == 'complete'] = "Complete"
test$name[test$name == 'mcquitty'] = "Mcquitty"

#save(list = 'test',file = 'cache//6000_all_clusters_2020//clusterTest.RData')
#load(file = 'cache//clusterTest.RData')
as.tibble(test) %>% filter(type == 'hierarchical')
gc()
gg1 = ggplot2::ggplot(data = as.tibble(test) %>% filter(type == 'hierarchical' ), mapping = aes(x = k,y = silhouette,colour = name)) +
  geom_line() +
  labs(x = 'Number of Clusters', y = 'Average. Silhouette Width', colour = 'Method') +
  theme(legend.title = element_text(size = axisTextSize),
        legend.text = element_text(size = axisTextSize),
        axis.text = element_text(size = axisTextSize), 
        axis.title = element_text(size = axisTitleSize),
        strip.text.x = element_text(size = axisTextSize)) 

# print(gg1)
ggsave(filename = paste0(plotsDir,"hierarchical-cluster-avg-silhouette-plot.png"), plot = gg1, width = 10, height = 8, dpi = 150)

gg2 = ggplot2::ggplot(data = as.tibble(test) %>% filter(type == 'hierarchical'), mapping = aes(x = k,y = elapsedTime,colour = name)) +
  geom_line() +
  ggtitle('Cluster Method') +
  labs(x = 'Number of Clusters', y = 'Elapsed Time', colour = 'Method') +
  theme(plot.title = element_text(hjust = 0.5), legend.position="none") + facet_grid(~name)
# print(gg2)
ggsave(filename = paste0(plotsDir,"hierarchical-cluster-time-plot.png"), plot = gg2, width = 12, height = 4, dpi = 150)

gg3 = ggplot2::ggplot(data = as.tibble(test) %>% filter(type == 'optimization'), mapping = aes(x = k,y = silhouette,colour = name)) +
  geom_line() +
  labs(x = 'Number of Clusters', y = 'Average. Silhouette Width', colour = 'Method') +
  theme(legend.title = element_text(size = axisTextSize),
        legend.text = element_text(size = axisTextSize),
        axis.text = element_text(size = axisTextSize), 
        axis.title = element_text(size = axisTitleSize),
        strip.text.x = element_text(size = axisTextSize)) 
# print(gg3)
ggsave(filename = paste0(plotsDir,"optimization-cluster-avg-silhouette-plot.png"), plot = gg3, width = 10, height = 8, dpi = 150)

gg4 = ggplot2::ggplot(data = as.tibble(test) %>% filter(type == 'optimization' ), mapping = aes(x = k,y = elapsedTime,colour = name)) +
  geom_line() +
  ggtitle('Cluster Method x Elapsed Time') +
  labs(x = 'Number of Clusters', y = 'Elapsed Time', colour = 'Method') +
  theme(plot.title = element_text(hjust = 0.5))
# print(gg4)
ggsave(filename = paste0(plotsDir,"optimization-cluster-time-plot.png"), plot = gg4, width = 10, height = 8, dpi = 150)


gc()

tables = list()
names = c('Ward.D','Ward.D2','Complete', 'Average','Single','Mcquitty','PAM','K-Medoids')
summaryTable = tibble(index = rep(1:times))
for (i in 1:length(names)){
  clusters = as.tibble(test) %>% filter(name == names[i])
  
    maxSilhouette = clusters$k[which(clusters$silhouette == max(clusters$silhouette) )[1]]
    
    curCluster = clusters$cluster[which(clusters$silhouette == max(clusters$silhouette) )[1]]
    
    print(paste0(names[i],':'))
    print(paste0('maxSilhouette: ',maxSilhouette))
    emails = rownames(matrixDistance)
    
    clustersTab = as.tibble(matrix(data = c(emails,curCluster[[1]]), ncol = 2, dimnames = list(rep(1:length(emails)),c('email','cluster'))))
    tags = t(sapply(X = clustersTab[,1]$email, FUN = getMainTags,usersTagsMatrix = usersTags,qtt = 5))
    clustersTab = add_column(.data = as.tibble(clustersTab),
                             tag1 = substr(x = tags[,2],start = 1,stop = 12),
                             tag2 = substr(x = tags[,3],start = 1,stop = 12),
                             tag3 = substr(x = tags[,4],start = 1,stop = 12),
                             tag4 = substr(x = tags[,5],start = 1,stop = 12),
                             tag5 = substr(x = tags[,6],start = 1,stop = 12),
                             .after = ncol(clustersTab))
    
    clustersTab = arrange(clustersTab,cluster,email)
    usersClusters = clustersTab %>% count(cluster) %>% arrange(desc(n)) %>% rename(users = n)
      
    finalClustersTab = filter(clustersTab,cluster == -1)
    for(j in 1:maxSilhouette){
      clusterTab = clustersTab %>% filter(cluster == j)
      indexes = sample(1:nrow(clusterTab),min(5,nrow(clusterTab)))
      finalClustersTab = bind_rows(finalClustersTab,slice(clusterTab,indexes))
    }
    tables[[i]] = list(clustersTab = finalClustersTab,
                       name = names[i],
                       usersClusters = usersClusters)
    print(usersClusters)
    usersClusters = usersClusters %>% arrange(-desc(as.numeric(cluster)))
    newColumn = as.list(bind_rows(usersClusters %>% select(users),tibble(users=rep(0,times - nrow(usersClusters %>% select(users))))))$users
    summaryTable = add_column(summaryTable,!!(gsub(pattern = '\\.',replacement = '',x = paste0(names[i],'-',maxSilhouette))) := newColumn)
    
  #   rTable = tables[[i]]$usersClusters
  #   rTable = rownames_to_column(rTable, var = " ")
  #   newTable = xtable::xtable(rTable)
  #   label(newTable) = paste0("tab:users-clusters-table")
  #   caption(newTable) = paste0("Users per clusters for ",paste(toupper(substr(tables[[i]]$name, 1, 1)), substr(tables[[i]]$name, 2, nchar(tables[[i]]$name)), sep=""), " cluster method.")
  #   align(newTable) = "|r|c|c|r|"
  #   print.xtable(newTable
  #                ,sanitize.colnames.function = function(x) return(paste0("\\cellcolor[HTML]{000000}{\\color[HTML]{FFFFFF}",x,"}"))
  #                ,include.rownames = FALSE
  #                ,file = paste0(tablesDir,"users-clusters-",tables[[i]]$name,"-table.tex")
  #                ,latex.environments = c('adjustbox')
  #                ,caption.placement = 'top'
  #                ,label.placement = 'top')
  #   
  #   rTable = tables[[i]]$clustersTab %>% select(cluster,tag1,tag2,tag3,tag4,tag5)
  #   rTable = rownames_to_column(rTable, var = " ")
  #   newTable = xtable::xtable(rTable)
  #   label(newTable) = paste0("tab:users-clusters-tags-table")
  #   caption(newTable) = paste0("Users versus tags for ",paste(toupper(substr(tables[[i]]$name, 1, 1)), substr(tables[[i]]$name, 2, nchar(tables[[i]]$name)), sep=""), " cluster method.")
  #   align(newTable) = "|r|r|c|p{2.5cm}|p{2.5cm}|p{2.5cm}|p{2.5cm}|p{2.5cm}|"
  #   
  #   print.xtable(newTable
  #                ,sanitize.colnames.function = function(x) return(paste0("\\cellcolor[HTML]{000000}{\\color[HTML]{FFFFFF}",x,"}"))
  #                ,include.rownames = FALSE
  #                ,hline.after = rep(1:nrow(rTable))
  #                ,file = paste0(tablesDir,"users-clusters-tags-",tables[[i]]$name,"-table.tex")
  #                ,latex.environments = c('adjustbox')
  #                ,caption.placement = 'top'
  #                ,label.placement = 'top')
  #     
  # 
  
}

View(summaryTable)

save()