set search_path to tablefunc,sample;
do $$
declare
    command varchar;
    c record;
begin
select concat('select * 
                 from crosstab('' select art.article_id,tg.name,count(1) 
                                    from articles ar 
                                   inner join articles_tags art on ar.id = art.article_id
                                   inner join tags tg on art.tag_id = tg.id
                                   group by tg.name, art.article_id
                                   order by tg.name, art.article_id limit 1599'',''select tg.name from tags tg order by tg.name limit 1599'')
                   as (article_id text,', string_agg(concat('"',tg.name,'"'),' text,'),' text)') into command 
  from tags tg order by 1 limit 1599;

select concat('COPY (', command,') TO ''/home/postgres/misc/file.csv''') into command;
--select 'select * from tags' into strict command;
execute command;

--execute 'select * from tags' into c

end $$;
