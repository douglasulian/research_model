-- Table: articles_raw

-- DROP TABLE articles_raw;

CREATE TABLE articles_raw
(
    data json
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE articles_raw
    OWNER to "cl-us-gzh";