-- Table: classifications

--DROP TABLE classifications;

CREATE TABLE classifications
(
    id text COLLATE pg_catalog."default" NOT NULL,
    path text COLLATE pg_catalog."default",
    CONSTRAINT classifications_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE classifications
    OWNER to "cl-us-gzh";

-- Index: classifications_id_idx

-- DROP INDEX classifications_id_idx;

CREATE INDEX classifications_id_idx
    ON classifications USING btree
    (id COLLATE pg_catalog."default")
    TABLESPACE pg_default;