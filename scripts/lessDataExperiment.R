options(warn = -1)
#### Install Packages ####
list.of.packages = c('tidyverse','dplyr','compiler','profvis','SPOT')
new.packages = list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if (length(new.packages)) install.packages(new.packages)
 
#### Libraries ####
# Loads tidyverse tools
library(tidyverse)
library(dplyr)
library(compiler)
library(profvis)
library(SPOT)

#### Import Functions ####
# debugSource('scripts/stemmer.R')
debugSource('scripts/cache.R')
debugSource('scripts/cluster.R')
debugSource('scripts/dataBase.R')
debugSource('scripts/distances.R')
debugSource('scripts/evaluation.R')
debugSource('scripts/plots.R')
debugSource('scripts/util.R')

#### Main ####
enableJIT(3)

# number of clusters must be less then or equal to the number of users available for clustering.
#            6000  2000   200
# articles   6681  8140  2272
# tags       4941 10849  4588
# users            1021   104

lessDataTest = function(trainningDataContent,
                        trainningDataBehavior,
                        testingData,
                        clusterMethod,
                        tagsMethod,
                        articlesMethod,
                        tagsCutGamaIndex,
                        articlesCutZetaIndex,
                        forgCurveLambdaIndex,
                        usersTimeDiffAlphaIndex,
                        mixedDistanceBetaIndex,
                        noClustersK){
  
  n = ncol(trainningDataBehavior$usersArticlesMatrix)
  auxMatrix = matrix(data = rep(1,n*n),nrow = n)
  auxMatrix[upper.tri(x = auxMatrix,diag = TRUE)] = 0
  behaviourDistanceTime = system.time({
    # calculates behaviour distance
    behaviourDistance = getJaccardDistance(usersArticlesMatrix           = trainningDataBehavior$usersArticlesMatrix,
                                           usersArticlesTimeDiffMatrix   = trainningDataBehavior$usersArticlesTimeDiffMatrix,
                                           articlesPopularityIndexVector = trainningDataBehavior$articlesPopularityIndexVector,
                                           alphaIndex = usersTimeDiffAlphaIndex)
    behaviourDistance = behaviourDistance * auxMatrix
    rm(list = c('auxMatrix'))
  })
  contentDistanceTime = system.time({
    # Applies forgetting curve over content (tags)
    usersTagsMatrix = getUsersTagsMatrix(usersArticlesMatrix          = trainningDataContent$usersArticlesMatrix,
                                         articlesTagsMatrix           = trainningDataContent$articlesTagsMatrix,
                                         usersArticlesTimeDiffMatrix  = trainningDataContent$usersArticlesTimeDiffMatrix,
                                         lambdaIndex                  = forgCurveLambdaIndex)
    # Calculates content distance
    contentDistance = getCosineDistance(usersTagsMatrix = usersTagsMatrix)
  })
  
  mixedDistanceTime = system.time({
    mixedDistance = mixedDistanceBetaIndex*behaviourDistance + contentDistance * (1 - mixedDistanceBetaIndex)
  })
  
  distances = list(contentDistance       = contentDistance, 
                    behaviourDistance     = behaviourDistance,
                    mixedDistance         = mixedDistance,
                    contentDistanceTime   = contentDistanceTime, 
                    behaviourDistanceTime = behaviourDistanceTime,
                    mixedDistanceTime     = mixedDistanceTime,
                    usersTagsMatrix       = usersTagsMatrix)
  
  
  distance  = distances$mixedDistance
  if (class(distances$data) == 'list') {
    usersTagsMatrix = distances$data$usersTagsMatrix
  } else {
    usersTagsMatrix = distances$usersTagsMatrix
  }
  cluster  = getCluster(distance = distance, k = noClustersK, method = clusterMethod)
  solution = evaluateSolution(testingData    = testingData, 
                              usersTagsM     = usersTagsMatrix, 
                              cluster        = cluster$cluster$cluster, 
                              clusterMethod  = clusterMethod, 
                              tagsMethod     = tagsMethod,
                              articlesMethod = articlesMethod,
                              alphaIndex     = usersTimeDiffAlphaIndex,
                              betaIndex      = mixedDistanceBetaIndex,
                              lambdaIndex    = forgCurveLambdaIndex,
                              gamaIndex      = tagsCutGamaIndex,
                              zetaIndex      = articlesCutZetaIndex,
                              k              = noClustersK) 
  
  return(solution)
}
  
  
cacheFile      = 'cache.RData'
cacheDir       = 'cache//6000_all_clusters_less_data'

clearConnections()

trainningConnection = getDataBaseConnection(schema = 'trainning6000', dbUser = "cl-us-gzh", dbHost = "10.238.4.109", dbName = "cl-us-gzh", dbPass = "cl-us-gzh")
testingConnection   = getDataBaseConnection(schema = 'testing6000'  , dbUser = "cl-us-gzh", dbHost = "10.238.4.109", dbName = "cl-us-gzh", dbPass = "cl-us-gzh")

trainningDataFull   = getTrainningData(trainningConnection)

#fromBehavior = getForgettingIndex(lambdaIndex = 0.1974,timeDifference = 60)
#trainningDataBehavior   = getTrainningDataFrom(trainningConnection,from=as.Date("01-05-2017", format = "%d-%m-%Y"))

getForgettingIndex(lambdaIndex = 0.1974,timeDifference = 27)
fromContent = as.Date(as.POSIXct(getEventsMaxTime(trainningConnection), origin="1970-01-01"))-27
trainningDataContent    = getTrainningDataFrom(trainningConnection,from=fromContent)

testingData             = getTestingData(testingConnection)

# solutionFull = lessDataTest(trainningDataContent = trainningDataFull,
#                             trainningDataBehavior = trainningDataFull,
#                             testingData = testingData,
#                             clusterMethod = 'complete',
#                             tagsMethod = 'index',
#                             articlesMethod = 'index',
#                             tagsCutGamaIndex = 0.9040,
#                             articlesCutZetaIndex = 0.5618,
#                             forgCurveLambdaIndex = 0.4511,
#                             usersTimeDiffAlphaIndex = 0.1974,
#                             mixedDistanceBetaIndex = 0.5244,
#                             noClustersK =16)

solutionLess = lessDataTest(trainningDataContent = trainningDataContent,
                            trainningDataBehavior = trainningDataFull,
                            testingData = testingData,
                            clusterMethod = 'complete',
                            tagsMethod = 'index',
                            articlesMethod = 'index',
                            tagsCutGamaIndex = 0.9040,
                            articlesCutZetaIndex = 0.5618,
                            forgCurveLambdaIndex = 0.4511,
                            usersTimeDiffAlphaIndex = 0.1974,
                            mixedDistanceBetaIndex = 0.5244,
                            noClustersK =16)
