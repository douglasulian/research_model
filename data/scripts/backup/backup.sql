PGDMP     &                	    u         	   cl-us-gzh    10.0    10.0 1               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           1262    16460 	   cl-us-gzh    DATABASE     {   CREATE DATABASE "cl-us-gzh" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE "cl-us-gzh";
          	   cl-us-gzh    false                        2615    16461    import    SCHEMA        CREATE SCHEMA import;
    DROP SCHEMA import;
          	   cl-us-gzh    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12980    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1255    16680    import()    FUNCTION     �  CREATE FUNCTION import() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
	result_classifications numeric default 0;    
	result_articles numeric default 0;
	result_events numeric default 0;
    result_users numeric default 0;
    result_newsletter_categories numeric default 0;
    result_newsletter_optin numeric default 0;
    
    results record;
    
BEGIN
    delete from classifications;
    delete from classification_path_levels;
    delete from articles;
    delete from articles_tags;
    delete from tags;
    delete from events;
    delete from users;
    delete from newsletter_categories;
    delete from users_newsletter_categories;
    
    
    select import_classifications() into result_classifications;
    select import_articles() into result_articles;
    select import_events() into result_events;
    select import_users() into result_users;
    select import_newsletter_categories() into result_newsletter_categories;
    select import_newsletter_optin() into result_newsletter_optin;
    
    raise info 'Results:';
    for results in     
    	select '  classifications'             as type, count(1) as qtt from classifications union
		select '  classification_path_levels'  as type, count(1) as qtt from classification_path_levels union 
		select '  articles'                    as type, count(1) as qtt from articles union 
		select '  articles_tags'               as type, count(1) as qtt from articles_tags union 
		select '  tags'                        as type, count(1) as qtt from tags union 
		select '  events'                      as type, count(1) as qtt from events union 
		select '  users'                       as type, count(1) as qtt from users union 
		select '  newsletter_categories'       as type, count(1) as qtt from newsletter_categories union
    	select '  users_newsletter_categories' as type, count(1) as qtt from users_newsletter_categories loop
		
        RAISE log '%: %', results.type,results.qtt;
        
    end loop;
    
    return 0;
END;
$$;
    DROP FUNCTION public.import();
       public       postgres    false    1    3            �            1255    16533    import_articles()    FUNCTION     �	  CREATE FUNCTION import_articles() RETURNS numeric
    LANGUAGE plpgsql
    AS $_$
DECLARE
    --subtotal ALIAS FOR $1;
    article record;
    tag record;
    tag_id numeric;
    total NUMERIC DEFAULT 0;
    parcial NUMERIC DEFAULT 0;
BEGIN
	SELECT count(1) INTO strict total FROM import.articles_raw;	

    for article in select 
        			data->>'body' as body,
        			data->>'tags-slugs' as tags_slugs,
        			data->>'published_first' as published_first,
        			data->>'exposed_id' as exposed_id,
        			data->>'all_classifications' as all_classifications,
        			data->>'created_date' as created_date,
        			data->>'external_id' as external_id,
        			data->>'id' as id
        			from import.articles_raw LOOP
        
        INSERT INTO public.articles(id, 
                                    body, 
                                    published_first, 
                                    exposed_id, 
                                    created_date, 
                                    external_id)
                            VALUES (nullif(trim(article.id),''),
                                    nullif(trim(article.body),''),
                                    to_date(article.published_first,'YYYY-MM-DD HH24:MI:SS'), --2017-05-09 08:02:06
                                    nullif(nullif(trim(article.exposed_id),''),'0'),
                                    to_date(article.created_date,'YYYY-MM-DD HH24:MI:SS'),
                                    nullif(nullif(trim(article.external_id),''),'0'));
                                    
        for tag in select value as name from json_array_elements_text(cast(article.tags_slugs AS json)) loop

            begin
                select id into strict tag_id from tags where name = tag.name;
            exception
                when NO_DATA_FOUND then
                    insert into tags (id,name) values (nextval('sq_tag_id'),tag.name);
                    tag_id := currval('sq_tag_id');
                --when TOO_MANY_ROWS then 
                --    raise exception 'tag % not unique', tag.name;
            end; 
            insert into articles_tags (article_id,tag_id) values (article.id,tag_id);
        end loop;
        --commit;
        parcial := parcial + 1;
        if (mod(parcial,round(total/100,0)) = 0 or parcial = total) then
        	raise log 'Articles: % percent imported',round(parcial/total*100,0);
        end if;
    end loop;
    RETURN 0;
END;
$_$;
 (   DROP FUNCTION public.import_articles();
       public       postgres    false    3    1            �            1255    36698    import_classifications()    FUNCTION     A  CREATE FUNCTION import_classifications() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    classification record;
    path_level numeric;
    path_level_name text;
    parcial numeric default 0;
    total numeric default 0;
begin
	select count(1) into strict total from import.classifications_raw;

    for classification in SELECT id, path FROM import.classifications_raw LOOP
    
        insert into classifications (id,path) 
                             values (nullif(trim(classification.id),''), 
                                     nullif(trim(classification.path),''));
    
        path_level := 0;
        
        foreach path_level_name in array regexp_split_to_array(classification.path,'[^/]+') loop
    		if (nullif(trim(path_level_name),'') is not null) then
            	insert into classification_path_levels (classification_id, 
                	                                    name, 
                    	                                level) 
                        	                    values (nullif(trim(classification.id),''), 
                            	                        nullif(trim(path_level_name),''),
                                	                    path_level);
                                	                    
            	path_level := path_level + 1;
            end if;
    	end loop;
        
    	parcial := parcial + 1;
        if (mod(parcial,round(total/100,0)) = 0 or parcial = total) then
        	raise log 'Classifications: % percent imported',round(parcial/total*100,0);
        end if;
	end loop;
    RETURN 0;
END;
$$;
 /   DROP FUNCTION public.import_classifications();
       public       postgres    false    3    1            �            1255    16640    import_events()    FUNCTION     >  CREATE FUNCTION import_events() RETURNS numeric
    LANGUAGE plpgsql
    AS $_$
DECLARE
    --subtotal ALIAS FOR $1;
    event record;
    --tag record;
    --tag_id numeric;
    total NUMERIC DEFAULT 0;
    parcial NUMERIC DEFAULT 0;
BEGIN
    SELECT count(1) INTO strict total FROM import.events_raw;
    
	for event in SELECT eventosid, 
                        data_evento, 
                        conteudo_topico, 
                        tag_evento, 
                        tipo_conteudo, 
                        email, 
                        categoria_conteudo, 
                        url_acessado, 
                        tipo_usuario, 
                        veiculo, 
                        dispositivo, 
                        id_materia, 
                        nome_edicao, 
                        data_edicao, 
                        numero_edicao, 
                        tipo_edicao
	               FROM import.events_raw LOOP
		INSERT INTO public.events(eventosid,
                                  data_evento,
                                  conteudo_topico,
                                  tag_evento,
                                  tipo_conteudo,
                                  email,
                                  categoria_conteudo,
                                  url_acessado,
                                  tipo_usuario,
                                  veiculo,
                                  dispositivo,
                                  id_materia,
                                  nome_edicao,
                                  data_edicao,
                                  numero_edicao,
                                  tipo_edicao,
                                  id_materia_extraido)
						  VALUES (to_number(event.eventosid,'9999999999'),
                                  to_date(event.data_evento,'DD-MM-YYYY'),
                                  nullif(trim(event.conteudo_topico),''),
                                  nullif(trim(event.tag_evento),''),
                                  nullif(trim(event.tipo_conteudo),''),
                                  nullif(trim(event.email),''),
                                  nullif(trim(event.categoria_conteudo),''),
                                  nullif(trim(event.url_acessado),''),
                                  nullif(trim(event.tipo_usuario),''),
                                  nullif(trim(event.veiculo),''),
                                  nullif(trim(event.dispositivo),''),
                                  nullif(nullif(trim(event.id_materia),'0'),''),
                                  nullif(trim(event.nome_edicao),''),
                                  to_date(event.data_edicao,'DD-MM-YYYY'),
                                  to_number(nullif(nullif(trim(event.numero_edicao),''),'0'),'999999'),
                                  nullif(trim(event.tipo_edicao),''),
                                  coalesce(nullif(nullif(trim(event.id_materia),'0'),''),substring(event.url_acessado from '[0-9]{6,7}'),substring(substring(event.url_acessado from '-[a-z|A-Z|0-9]{25}\.html') from 2 for 25)));
        parcial := parcial + 1;
        if (mod(parcial,round(total/100,0)) = 0 or parcial = total) then
        	raise log 'Events: % percent imported',round(parcial/total*100,0);
        end if;
    end loop;
    RETURN 0;
END;
$_$;
 &   DROP FUNCTION public.import_events();
       public       postgres    false    1    3            �            1255    36670    import_newsletter_categories()    FUNCTION     ~  CREATE FUNCTION import_newsletter_categories() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    
    insert into newsletter_categories (id,name) values ('1', 'Gremista ZH');
    insert into newsletter_categories (id,name) values ('2', 'Colorado ZH');
    insert into newsletter_categories (id,name) values ('3', 'Colunistas ZH');
    insert into newsletter_categories (id,name) values ('4', 'Destaques da Manhã');
    insert into newsletter_categories (id,name) values ('5', 'Destaques do Editor');
    insert into newsletter_categories (id,name) values ('6', 'Destemperados');
    insert into newsletter_categories (id,name) values ('7', 'Donna');
    insert into newsletter_categories (id,name) values ('8', 'Encare a Crise');
    insert into newsletter_categories (id,name) values ('9', 'Especiais ZH');
    insert into newsletter_categories (id,name) values ('10', 'ZH Doc');
    insert into newsletter_categories (id,name) values ('11', 'ZH Findi');
    insert into newsletter_categories (id,name) values ('12', 'ZH Viagem');
    insert into newsletter_categories (id,name) values ('13', 'ZH Vida');
    
    RETURN 0;
END;
$$;
 5   DROP FUNCTION public.import_newsletter_categories();
       public       postgres    false    3    1            �            1255    16674    import_newsletter_optin()    FUNCTION     =  CREATE FUNCTION import_newsletter_optin() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    optin record;
    user_exists numeric default 0;
    parcial numeric default 0;
    total numeric default 0;
begin
	select count(1) into strict total from import.newsletter_optins_raw;
	
    for optin in SELECT email, 
                        data_optin, 
                        ultima_alteracao,
                        fg_optout,
                        colorado_zh,
                        colunistas_zh,
                        destaques_da_manha,
                        destaques_do_editor,
                        destemperados,
                        donna,
                        encare_a_crise,
                        especiais_zh,
                        gremista_zh,
                        zh_doc,
                        zh_findi,
                        zh_viagem,
                        zh_vida
	FROM import.newsletter_optins_raw LOOP
        --update user details
        begin
        	--select 1 into strict user_exists from public.users us where us.email = trim(optin.email);
            update public.users set newsletter_optin = 1,
                        newsletter_optin_date  = to_date(              optin.data_optin,'DD.MM.YYYY HH24:MI:SS'),
                        newsletter_optout      = to_number(nullif(trim(optin.fg_optout),''),'9'),
                        newsletter_last_update = to_date(              optin.ultima_alteracao,'DD.MM.YYYY HH24:MI:SS')
             where email = trim(optin.email);
            if trim(optin.gremista_zh        ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'1' ); end if;
            if trim(optin.colorado_zh        ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'2' ); end if;
            if trim(optin.colunistas_zh      ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'3' ); end if;
            if trim(optin.destaques_da_manha ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'4' ); end if;
            if trim(optin.destaques_do_editor) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'5' ); end if;
            if trim(optin.destemperados      ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'6' ); end if;
            if trim(optin.donna              ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'7' ); end if;
            if trim(optin.encare_a_crise     ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'8' ); end if;
            if trim(optin.especiais_zh       ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'9' ); end if;
            if trim(optin.zh_doc             ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'10'); end if;
            if trim(optin.zh_findi           ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'11'); end if;
            if trim(optin.zh_viagem          ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'12'); end if;
            if trim(optin.zh_vida            ) = '1' then insert into users_newsletter_categories (user_id, newsletter_category_id) values (trim(optin.email),'13'); end if;
        exception when NO_DATA_FOUND then
            raise log 'User not found: %', trim(optin.email);
        end;
        
        parcial := parcial + 1;
        if (mod(parcial,round(total/100,0)) = 0 or parcial = total) then
        	raise log 'Newsletter Optins: % percent imported',round(parcial/total*100,0);
        end if;
    end loop;
    RETURN 0;
END;
$$;
 0   DROP FUNCTION public.import_newsletter_optin();
       public       postgres    false    3    1            �            1255    16679    import_users()    FUNCTION       CREATE FUNCTION import_users() RETURNS numeric
    LANGUAGE plpgsql
    AS $$
DECLARE
    us record;
    
BEGIN
	
    insert into public.users (email) SELECT DISTINCT email FROM public.events where nullif(trim(email),'') is not null;
    
    RETURN 0;
END;
$$;
 %   DROP FUNCTION public.import_users();
       public       postgres    false    1    3            �            1259    26461    articles_raw    TABLE     -   CREATE TABLE articles_raw (
    data json
);
     DROP TABLE import.articles_raw;
       import      	   cl-us-gzh    false    4            �            1259    36641    classifications_raw    TABLE     A   CREATE TABLE classifications_raw (
    id text,
    path text
);
 '   DROP TABLE import.classifications_raw;
       import      	   cl-us-gzh    false    4            �            1259    24576 
   events_raw    TABLE       CREATE TABLE events_raw (
    eventosid text,
    data_evento text,
    conteudo_topico text,
    tag_evento text,
    tipo_conteudo text,
    email text,
    categoria_conteudo text,
    url_acessado text,
    tipo_usuario text,
    veiculo text,
    dispositivo text,
    id_materia text,
    nome_edicao text,
    data_edicao text,
    numero_edicao text,
    tipo_edicao text
);
    DROP TABLE import.events_raw;
       import      	   cl-us-gzh    false    4            �            1259    24585    newsletter_optins_raw    TABLE       CREATE TABLE newsletter_optins_raw (
    email text,
    ret_email text,
    data_optin text,
    ultima_alteracao text,
    fg_hardbounce text,
    fg_optout text,
    motivo_optout text,
    outrosmotivos text,
    primeironome text,
    ultimonome text,
    confirmado text,
    formatopreferido text,
    clube_do_assinante text,
    colorado_zh text,
    colunistas_zh text,
    destaques_da_manha text,
    destaques_do_editor text,
    destemperados text,
    donna text,
    encare_a_crise text,
    especiais_zh text,
    gremista_zh text,
    zh_doc text,
    zh_findi text,
    zh_viagem text,
    zh_vida text,
    fg_lead text,
    nome_lead text,
    cpf_lead text,
    dt_nasc_lead text,
    telefone_lead text,
    cidade_lead text,
    origem_lead text
);
 )   DROP TABLE import.newsletter_optins_raw;
       import      	   cl-us-gzh    false    4            �            1259    264141    articles    TABLE     �   CREATE TABLE articles (
    id text NOT NULL,
    body text,
    published_first date,
    exposed_id text,
    created_date date,
    external_id text
);
    DROP TABLE public.articles;
       public      	   cl-us-gzh    false    3            �            1259    16516    articles_tags    TABLE     Z   CREATE TABLE articles_tags (
    article_id text NOT NULL,
    tag_id numeric NOT NULL
);
 !   DROP TABLE public.articles_tags;
       public      	   cl-us-gzh    false    3            �            1259    36699    classification_path_levels    TABLE     �   CREATE TABLE classification_path_levels (
    name text NOT NULL,
    level numeric NOT NULL,
    classification_id text NOT NULL
);
 .   DROP TABLE public.classification_path_levels;
       public      	   cl-us-gzh    false    3            �            1259    36685    classifications    TABLE     F   CREATE TABLE classifications (
    id text NOT NULL,
    path text
);
 #   DROP TABLE public.classifications;
       public      	   cl-us-gzh    false    3            �            1259    77015    events    TABLE     �  CREATE TABLE events (
    eventosid numeric NOT NULL,
    data_evento date NOT NULL,
    conteudo_topico text,
    tag_evento text,
    tipo_conteudo text,
    email text NOT NULL,
    categoria_conteudo text,
    url_acessado text,
    tipo_usuario text,
    veiculo text,
    dispositivo text,
    id_materia text,
    nome_edicao text,
    data_edicao date,
    numero_edicao numeric,
    tipo_edicao text,
    id_materia_extraido text
);
    DROP TABLE public.events;
       public      	   cl-us-gzh    false    3            �            1259    36655    newsletter_categories    TABLE     U   CREATE TABLE newsletter_categories (
    id text NOT NULL,
    name text NOT NULL
);
 )   DROP TABLE public.newsletter_categories;
       public      	   cl-us-gzh    false    3            �            1259    640229 	   sq_tag_id    SEQUENCE     j   CREATE SEQUENCE sq_tag_id
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;
     DROP SEQUENCE public.sq_tag_id;
       public    	   cl-us-gzh    false    3            �            1259    16508    tags    TABLE     >   CREATE TABLE tags (
    id numeric,
    name text NOT NULL
);
    DROP TABLE public.tags;
       public      	   cl-us-gzh    false    3            �            1259    16581    users    TABLE     �   CREATE TABLE users (
    email text NOT NULL,
    newsletter_optin_date date,
    newsletter_last_update date,
    newsletter_optin numeric(1,0),
    newsletter_optout numeric(1,0)
);
    DROP TABLE public.users;
       public      	   cl-us-gzh    false    3            �            1259    36664    users_newsletter_categories    TABLE     r   CREATE TABLE users_newsletter_categories (
    user_id text NOT NULL,
    newsletter_category_id text NOT NULL
);
 /   DROP TABLE public.users_newsletter_categories;
       public      	   cl-us-gzh    false    3            t          0    26461    articles_raw 
   TABLE DATA               %   COPY articles_raw (data) FROM stdin;
    import    	   cl-us-gzh    false    202   �o       u          0    36641    classifications_raw 
   TABLE DATA               0   COPY classifications_raw (id, path) FROM stdin;
    import    	   cl-us-gzh    false    203   �o       r          0    24576 
   events_raw 
   TABLE DATA               �   COPY events_raw (eventosid, data_evento, conteudo_topico, tag_evento, tipo_conteudo, email, categoria_conteudo, url_acessado, tipo_usuario, veiculo, dispositivo, id_materia, nome_edicao, data_edicao, numero_edicao, tipo_edicao) FROM stdin;
    import    	   cl-us-gzh    false    200   �o       s          0    24585    newsletter_optins_raw 
   TABLE DATA               �  COPY newsletter_optins_raw (email, ret_email, data_optin, ultima_alteracao, fg_hardbounce, fg_optout, motivo_optout, outrosmotivos, primeironome, ultimonome, confirmado, formatopreferido, clube_do_assinante, colorado_zh, colunistas_zh, destaques_da_manha, destaques_do_editor, destemperados, donna, encare_a_crise, especiais_zh, gremista_zh, zh_doc, zh_findi, zh_viagem, zh_vida, fg_lead, nome_lead, cpf_lead, dt_nasc_lead, telefone_lead, cidade_lead, origem_lead) FROM stdin;
    import    	   cl-us-gzh    false    201   �o       {          0    264141    articles 
   TABLE DATA               ]   COPY articles (id, body, published_first, exposed_id, created_date, external_id) FROM stdin;
    public    	   cl-us-gzh    false    209   �o       p          0    16516    articles_tags 
   TABLE DATA               4   COPY articles_tags (article_id, tag_id) FROM stdin;
    public    	   cl-us-gzh    false    198   p       y          0    36699    classification_path_levels 
   TABLE DATA               M   COPY classification_path_levels (name, level, classification_id) FROM stdin;
    public    	   cl-us-gzh    false    207   2p       x          0    36685    classifications 
   TABLE DATA               ,   COPY classifications (id, path) FROM stdin;
    public    	   cl-us-gzh    false    206   Op       z          0    77015    events 
   TABLE DATA                 COPY events (eventosid, data_evento, conteudo_topico, tag_evento, tipo_conteudo, email, categoria_conteudo, url_acessado, tipo_usuario, veiculo, dispositivo, id_materia, nome_edicao, data_edicao, numero_edicao, tipo_edicao, id_materia_extraido) FROM stdin;
    public    	   cl-us-gzh    false    208   lp       v          0    36655    newsletter_categories 
   TABLE DATA               2   COPY newsletter_categories (id, name) FROM stdin;
    public    	   cl-us-gzh    false    204   �p       o          0    16508    tags 
   TABLE DATA               !   COPY tags (id, name) FROM stdin;
    public    	   cl-us-gzh    false    197   �p       q          0    16581    users 
   TABLE DATA               s   COPY users (email, newsletter_optin_date, newsletter_last_update, newsletter_optin, newsletter_optout) FROM stdin;
    public    	   cl-us-gzh    false    199   �p       w          0    36664    users_newsletter_categories 
   TABLE DATA               O   COPY users_newsletter_categories (user_id, newsletter_category_id) FROM stdin;
    public    	   cl-us-gzh    false    205   �p       �           0    0 	   sq_tag_id    SEQUENCE SET     1   SELECT pg_catalog.setval('sq_tag_id', 0, false);
            public    	   cl-us-gzh    false    210            �
           2606    264148    articles articles_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.articles DROP CONSTRAINT articles_pkey;
       public      	   cl-us-gzh    false    209            �
           2606    36692 $   classifications classifications_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY classifications
    ADD CONSTRAINT classifications_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.classifications DROP CONSTRAINT classifications_pkey;
       public      	   cl-us-gzh    false    206            �
           2606    77022    events events_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (eventosid);
 <   ALTER TABLE ONLY public.events DROP CONSTRAINT events_pkey;
       public      	   cl-us-gzh    false    208            �
           2606    36662 0   newsletter_categories newsletter_categories_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY newsletter_categories
    ADD CONSTRAINT newsletter_categories_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.newsletter_categories DROP CONSTRAINT newsletter_categories_pkey;
       public      	   cl-us-gzh    false    204            �
           2606    16515    tags tags_pkey 
   CONSTRAINT     G   ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (name);
 8   ALTER TABLE ONLY public.tags DROP CONSTRAINT tags_pkey;
       public      	   cl-us-gzh    false    197            �
           2606    16590    users user_pkey 
   CONSTRAINT     I   ALTER TABLE ONLY users
    ADD CONSTRAINT user_pkey PRIMARY KEY (email);
 9   ALTER TABLE ONLY public.users DROP CONSTRAINT user_pkey;
       public      	   cl-us-gzh    false    199            t      x������ � �      u      x������ � �      r      x������ � �      s      x������ � �      {      x������ � �      p      x������ � �      y      x������ � �      x      x������ � �      z      x������ � �      v      x������ � �      o      x������ � �      q      x������ � �      w      x������ � �     