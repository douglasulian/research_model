CREATE TABLE events (
    eventosid numeric NULL,
    data_evento timestamptz NULL,
    conteudo_topico text NULL,
    tag_evento text NULL,
    tipo_conteudo text NULL,
    email text NULL,
    categoria_conteudo text NULL,
    url_acessado text NULL,
    tipo_usuario text NULL,
    veiculo text NULL,
    dispositivo text NULL,
    id_materia text NULL,
    nome_edicao text NULL,
    data_edicao date NULL,
    numero_edicao numeric NULL,
    tipo_edicao text NULL,
    id_materia_extraido text NULL,
    article_id text NULL
)
WITH (
    OIDS=FALSE
) ;
create
    index events_article_id_idx on
    events
        using btree(article_id) ;
create
    index events_email_idx on
    events
        using btree(email) ;
create
    index events_eventosid_idx on
    events
        using btree(eventosid) ;
create
    index events_id_materia_idx on
    events
        using btree(id_materia) ;
create
    index events_url_acessado_idx on
    events
        using btree(url_acessado) ;
