-- Table: users

--DROP TABLE users;

CREATE TABLE users
(
    email text COLLATE pg_catalog."default" NOT NULL,
    newsletter_optin_date date,
    newsletter_last_update date,
    newsletter_optin numeric(1,0),
    newsletter_optout numeric(1,0),
    CONSTRAINT user_pkey PRIMARY KEY (email)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE users
    OWNER to "cl-us-gzh";

-- Index: users_email_idx

-- DROP INDEX users_email_idx;

CREATE INDEX users_email_idx
    ON users USING btree
    (email COLLATE pg_catalog."default")
    TABLESPACE pg_default;