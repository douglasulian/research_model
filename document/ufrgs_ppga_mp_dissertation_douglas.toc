\changetocdepth {4}
\babel@toc {english}{}
\babel@toc {english}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introduction}}{18}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{18}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{20}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}General Objective}{20}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Specific Objectives}{20}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Study relevance}{20}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Grupo RBS, Zero Hora and the Media Business}}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Media Industry}{21}{section.2.1}
\contentsline {section}{\numberline {2.2}Grupo RBS}{21}{section.2.2}
\contentsline {section}{\numberline {2.3}Zero Hora}{22}{section.2.3}
\contentsline {section}{\numberline {2.4}Consumer Understanding at Zero Hora}{22}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Related Work}}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}News Data Mining}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}News Recommendation}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Collaborative Filtering}{25}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Content-based Filtering}{26}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Hybrid approaches}{26}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Main Challenges}{26}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Cluster Analysis}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Similarity Measure}{28}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Number of Clusters}{30}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Clusters Validation}{32}{subsection.3.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Method}}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Objects Selection}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Variable Selection}{34}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}User Data Variables}{34}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Behavior Data Variables}{35}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Content Data Variables}{35}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Proximity Measure}{36}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Behavior Distance}{37}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Content Distance}{39}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Mixed Distance}{40}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Cluster Method and Evaluation}{40}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Optimization Methods Selection}{40}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Hierarchical Methods Selection}{41}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Number of Clusters}{42}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Cluster Effect Evaluation}{42}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}Replication and Testing}{43}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Selecting the recommended articles}{45}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Defining a metric}{45}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Parameters Optimization}{48}{subsection.4.5.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Implementation}}{50}{chapter.5}
\contentsline {section}{\numberline {5.1}Infrastructure}{50}{section.5.1}
\contentsline {section}{\numberline {5.2}Data import and transformation}{51}{section.5.2}
\contentsline {section}{\numberline {5.3}R Script}{56}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Distances Calculation}{56}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Clustering}{60}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Solution Evaluation}{60}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Optimization Process Performance}{61}{subsection.5.3.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Results}}{63}{chapter.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Conclusion}}{70}{chapter.7}
\contentsline {subsection}{\numberline {7.0.1}Recommendations and future studies}{72}{subsection.7.0.1}
\vspace {\cftbeforechapterskip }
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {References}}{74}{section*.40}
\vspace {\cftbeforechapterskip }
