\changetocdepth {4}
\select@language {brazil}
\select@language {english}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introduction}}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Context}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}General Objectives}{8}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Specific Objectives}{8}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Study relevance}{9}{section.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Grupo RBS, Zero Hora and the Media Business in Brazil}}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Media Business in Brazil}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Grupo RBS}{11}{section.2.2}
\contentsline {section}{\numberline {2.3}Zero Hora}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Customer Experience at Zero Hora}{12}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Related Work}}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}News Data Mining}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}News Recommendation Systems}{16}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Collaborative Filtering}{16}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Content-based Filtering}{17}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Hybrid approaches}{17}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Main Challenges}{17}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Cluster Analysis}{18}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Measure of Similarity}{20}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Number of Clusters}{22}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Clusters Validation}{23}{subsection.3.3.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Method}}{25}{chapter.4}
\contentsline {section}{\numberline {4.1}Objects Selection}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Variable Selection}{26}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}User Data Variables}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Behaviour Data Variables}{27}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Content Data Variables}{28}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Proximity Measure}{29}{section.4.3}
\contentsline {section}{\numberline {4.4}Cluster Method and Evaluation}{30}{section.4.4}
\contentsline {section}{\numberline {4.5}Replication and Testing}{30}{section.4.5}
\contentsline {section}{\numberline {4.6}System Overview}{31}{section.4.6}
\contentsline {section}{\numberline {4.7}Analysis}{34}{section.4.7}
\vspace {\cftbeforechapterskip }
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Bibliography}}{35}{section*.6}
\vspace {\cftbeforechapterskip }
